FROM golang:1.17.3

RUN git clone https://github.com/adterskov/geekbrains-conteinerization.git
RUN cd geekbrains-conteinerization/homework/2.docker/golang/ && \
    go build -o /go/golang -ldflags '-linkmode external -w -extldflags "-static"' .

FROM alpine:3.15.0
COPY --from=0 /go/golang /app
EXPOSE 8080
CMD /app
